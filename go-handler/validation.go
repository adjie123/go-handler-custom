package handler

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
)

// these variable should be constant
var (
	NameTitleWithNumeric string = `^[a-zA-Z]?[a-zA-Z0-9\.\,\s]+$`
	isNumeric                   = validation.Match(regexp.MustCompile("^[0-9]+$"))
	isAlphaNum                  = validation.Match(regexp.MustCompile("^[a-zA-Z0-9!@#$&()\\-`.+,/\"]*$"))
	isNameTitle                 = validation.Match(regexp.MustCompile(NameTitleWithNumeric))
	isDate                      = validation.Date("2006-01-02")
)
